# Team Guidelines

At the end of the project, you will be asked to provide feedback to your instructors about each individual team member’s contribution to your group. Each member of your group will also provide feedback on you. A portion of the final project grade will be based on this feedback.

**We are assessing our teammates based on their level of participation and cooperation, not on their individual coding skill level.**

You should receive positive feedback if you do these things:
- In group projects, the assessment is *more about working as a real, professional team than getting the project itself to 100% completion.* In the real world, you usually won't be able to decide who gets to be on your team, and you won't get away with ignoring them or failing to cooperate. Figure it out. Even if you know you're right, *the health of the team is more important than correctness in the project or in the process* you're following.
- Update the team daily on your status
- Communicate ahead of time what your schedule is and any changes in your schedule
- Participate in all planned team activities
- Communicate your strengths/interests/abilities/weaknesses
- Speak up about your concerns/confusion. Ask for clarification
- Don’t let your team do sloppy work, but also find compromise
- Invite teammates to give their ideas/opinions
- Actively listen to teammates ideas and consider pros/cons as a group
- Accept decisions made by the group (this may take the project in a direction you didn’t expect)
- If you are stuck on a problem - ask for help from your coaches/instructors/teammates ASAP (don’t waste time struggling individually - this is a team effort)
- Compliment individuals when they do a good job/show good effort. 
- Apologize if/when you screw up - Be real
  - “Really sorry guys, I fell asleep and missed our meeting. Can I sync up with someone later to get caught up on what I missed?”
  - “I broke the code sorry! Can we work together to try and fix it?”
- Follow-through to make up any missed deadlines/lack of participation
  - “I had some personal issues last week, so I will work extra hard this week to make up”
- **Be respectful to everyone**

### Some personality types to avoid:
- The Dictator
  - We will do it like this. No discussion.
- The Coaster
  - I’m good with whatever, man. Totally apathetic.
- The Ghost
  - I’m around sometimes. I do stuff then I disappear for awhile.
- The Nitpicker
  - Everything you do is *just* ok. I would have done it differently.
- The Scaredy-Cat
  - I stick to my corner and don’t take on challenges.
	
**Personal/group issues should be escalated to your instructors**
